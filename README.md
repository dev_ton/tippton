# TippTon Bot



## Installation and local launch

1. Launch a [mongo database](https://www.mongodb.com/) locally
2. Create `.env` file with the environment variables listed below
3. Install `ffmpeg` on your machine
4. Run `yarn` in the root folder
5. Run `yarn start`

## Environment variables in `.env` file

| Variable        | Description                                                     |
| --------------- | --------------------------------------------------------------- |
| `MONGO`         | URI for the mongo database used                                 |
| `TOKEN`         | Telegram bot token                                              |
| `SALT`          | Random salt to generate various encrypted stuff                 |
| `ADMIN_ID`      | Chat id of the person who shall receive valuable logs           |
| `WIT_LANGUAGES` | A map of language names to Wit.ai tokens                        |
| `ENVIRONMENT`   | App environment, can be `development`, defaults to `production` |

See examples in `.env.sample` file.
