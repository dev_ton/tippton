import { I18n } from '@grammyjs/i18n'

const i18n = new I18n({
  defaultLanguageOnMissing: true,
  directory: `${__dirname}/../../locales`,
  defaultLanguage: 'de',
})

export default i18n
