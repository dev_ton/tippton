import Context from '@/models/Context'
import logAnswerTime from '@/helpers/logAnswerTime'

const gitReference = process.env.GIT_REV

const source_url = `https://gitlab.com/dev_ton/tippton/`

export default async function handleHelp(ctx: Context) {
  await ctx.reply(ctx.i18n.t('help', {
    source_url: source_url,
    source_short_ref: gitReference.substr(0, 8)
  }), {
    disable_web_page_preview: true,
    parse_mode: 'Markdown'
  })
  logAnswerTime(ctx, '/help')
}
