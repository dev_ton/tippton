import { wit } from '@/engines/wit'

const engines = {
  wit,
}

export default engines
