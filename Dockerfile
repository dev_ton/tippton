FROM node:16-bullseye-slim

WORKDIR /usr/src/app

## following 3 lines are for installing ffmepg
RUN apt update && apt install ffmpeg -y

COPY package*.json ./
COPY yarn.lock ./

# install modules
RUN yarn

COPY . .

CMD [ "npm", "start" ]
